package com.bos.book.dao;


import com.bos.book.model.Order;

/**
 * 
 * @author senthilnps
 *
 */
public interface BookDao {
	void orderBook(Order order);
	void cancelBook(Order order);
	void updateBook(Order order);
}
