package com.bos.book;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ResourceLoader;

@Configuration
//@PropertySource({
//	"classpath:config/db.properties" 
//})
public class AppConfig {
	private static final Logger log = LoggerFactory.getLogger(AppConfig.class);

	@Value("${jdbc_driverClassName:driverClass_DefValue}")
	private String driverClassName;
	@Value("${jdbc_url:jdbcurl_DefValue}")
	private String jdbcURL;
	@Value("${jdbc_username:userName_DefValue}")
	private String username;
	@Value("${jdbc_secret:Secret_DefValue}")
	private String password;
	@Autowired
	private ResourceLoader resourceLoader;

	/*
	 * @Autowired private PlatformTransactionManager txnManager;
	 */
	@Bean
	public DataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();

		dataSource.setDriverClassName(driverClassName);
		dataSource.setUrl(jdbcURL);
		dataSource.setUsername(username);
		dataSource.setPassword(password);

		log.debug("username:" + username);
		return dataSource;
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	
}


