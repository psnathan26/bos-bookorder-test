package com.bos.book.exception;

import java.util.Map;

public class BookException
extends RuntimeException
{
private static final long serialVersionUID = 1L;
private Map<String, Object> returnMap;

public BookException() {}

public BookException(String message)
{
  super(message);
}

public BookException(String message, Map<String, Object> returnMap)
{
  super(message);
  this.returnMap = returnMap;
}

public BookException(String message, Throwable throwable)
{
  super(message, throwable);
}

public BookException(Throwable throwable)
{
  super(throwable.getMessage(), throwable);
}

public Map<String, Object> getReturnMap()
{
  return this.returnMap;
}
}
