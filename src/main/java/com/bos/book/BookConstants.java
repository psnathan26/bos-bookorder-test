package com.bos.book;

public class BookConstants {
	
	public static final String REQUEST_HEADER_TIME_STAMP = "timestamp";
	
	public static final String DATE_FORMAT="dd/MM/yyyy";
	public static final String DATE_TIME_FORMAT="yyyy-MM-dd'T'HH:mm:ss";
	
	public static final String RESPONSE_CODE_FAILURE="1";
	
	public static final String RESPONSE_CODE_SUCCESS="0";
	
}
