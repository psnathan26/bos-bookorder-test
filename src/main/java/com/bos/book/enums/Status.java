package com.bos.book.enums;

public enum Status {
	
	SUCCESS(1, "success"),  FAILED(0, "failed");

	private Status(int value, String desc)
	{
		this.value = value;
	}
	
	private int value;
	private String desc;
	
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
}
