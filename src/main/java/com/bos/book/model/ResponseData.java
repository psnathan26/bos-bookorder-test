package com.bos.book.model;

import java.util.ArrayList;
import java.util.List;

import com.bos.book.enums.ResultAction;
import com.bos.book.enums.Status;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseData<T> {
	//@JsonIgnore
	private transient String dummy;
	public static final String RESULT_LIST = "resultList";
	public static final String SEARCH_RESULT_CNT = "searchresultcnt";
	private T response;

	public T getResponse() {
		return response;
	}

	public void setResponse(T response) {
		this.response = response;
	}

	//@JsonIgnore
	private transient List<String> warningList = new ArrayList();

	//@JsonIgnore
	private transient String errorMessage;

	//@JsonIgnore
	private transient int status = Status.FAILED.getValue();

	//@JsonIgnore
	private transient ResultAction resultAction;

	public void setDummy(String dummy) {
		this.dummy = dummy;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setResultAction(ResultAction resultAction) {
		this.resultAction = resultAction;
	}

	public void setWarningList(List<String> warningList) {
		this.warningList = warningList;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof ResponseData)) {
			return false;
		}
		ResponseData<?> other = (ResponseData) o;
		if (!other.canEqual(this)) {
			return false;
		}
		Object this$dummy = getDummy();
		Object other$dummy = other.getDummy();
		if (this$dummy == null ? other$dummy != null : !this$dummy.equals(other$dummy)) {
			return false;
		}
		Object this$returnData = getResponse();
		Object other$returnData = other.getResponse();
		if (this$returnData == null ? other$returnData != null : !this$returnData.equals(other$returnData)) {
			return false;
		}
		if (getStatus() != other.getStatus()) {
			return false;
		}
		Object this$resultAction = getResultAction();
		Object other$resultAction = other.getResultAction();
		if (this$resultAction == null ? other$resultAction != null : !this$resultAction.equals(other$resultAction)) {
			return false;
		}
		Object this$warningList = getWarningList();
		Object other$warningList = other.getWarningList();
		if (this$warningList == null ? other$warningList != null : !this$warningList.equals(other$warningList)) {
			return false;
		}
		Object this$errorMessage = getErrorMessage();
		Object other$errorMessage = other.getErrorMessage();
		return this$errorMessage == null ? other$errorMessage == null : this$errorMessage.equals(other$errorMessage);
	}

	protected boolean canEqual(Object other) {
		return other instanceof ResponseData;
	}

	public int hashCode() {
		int PRIME = 59;
		int result = 1;
		Object $dummy = getDummy();
		result = result * 59 + ($dummy == null ? 43 : $dummy.hashCode());
		Object $returnData = getResponse();
		result = result * 59 + ($returnData == null ? 43 : $returnData.hashCode());
		result = result * 59 + getStatus();
		Object $resultAction = getResultAction();
		result = result * 59 + ($resultAction == null ? 43 : $resultAction.hashCode());
		Object $warningList = getWarningList();
		result = result * 59 + ($warningList == null ? 43 : $warningList.hashCode());
		Object $errorMessage = getErrorMessage();
		result = result * 59 + ($errorMessage == null ? 43 : $errorMessage.hashCode());
		return result;
	}

	public String toString() {
		return "ResponseData(dummy=" + getDummy() + ", returnData=" + getResponse() + ", status=" + getStatus()
				+ ", resultAction=" + getResultAction() + ", warningList=" + getWarningList() + ", errorMessage="
				+ getErrorMessage() + ")";
	}

	public String getDummy() {
		return this.dummy;
	}

	public int getStatus() {
		return this.status;
	}

	public ResultAction getResultAction() {
		return this.resultAction;
	}

	public List<String> getWarningList() {
		return this.warningList;
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	public ResponseData(Status status) {
		this.status = status.getValue();
	}

	public ResponseData(ResultAction resultAction) {
		this.resultAction = resultAction;
		this.status = Status.SUCCESS.getValue();
	}

	public ResponseData(T result) {
		this.response = result;
		this.status = Status.SUCCESS.getValue();
	}

	public void setStatusEnum(Status status) {
		setStatus(status.getValue());
	}

	public ResponseData() {
	}
}
