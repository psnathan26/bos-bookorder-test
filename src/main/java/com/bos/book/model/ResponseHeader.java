package com.bos.book.model;

import java.io.Serializable;

public class ResponseHeader implements Serializable
 
{
  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;
  
  private String msgId;
  private String serviceRequestId;
  private String timestamp;
  private String appId;
  
public String getMsgId() {
	return msgId;
}
public void setMsgId(String msgId) {
	this.msgId = msgId;
}
public String getTimestamp() {
	return timestamp;
}
public void setTimestamp(String timestamp) {
	this.timestamp = timestamp;
}
public String getAppId() {
	return appId;
}
public void setAppId(String appId) {
	this.appId = appId;
}
public static long getSerialversionuid() {
	return serialVersionUID;
}
public String getServiceRequestId() {
	return serviceRequestId;
}
public void setServiceRequestId(String serviceRequestId) {
	this.serviceRequestId = serviceRequestId;
}


 
}
