package com.bos.book.model;

public class Order
{
	
	private int orderQuantity;
    
    private String instrumentId;
    
	private double instrumentPrice;
    
    private String  orderDate;
    
    private String itemName;
    
   
	public Order(String name)
    {
  
        this.itemName = name;
    } 
    public Order(int orderQuantity, double instrumentPrice) {
		super();
		this.orderQuantity = orderQuantity;
		this.instrumentPrice = instrumentPrice;
	}

     
	public Order(int orderQuantity, String instrumentId, double instrumentPrice, String orderDate) {
		super();
		this.orderQuantity = orderQuantity;
		this.instrumentId = instrumentId;
		this.instrumentPrice = instrumentPrice;
		this.orderDate = orderDate;
	}
	
	 public String getItemName() {
			return itemName;
		}
		public void setItemName(String itemName) {
			this.itemName = itemName;
		}

	@Override
	public String toString() {
		return "Order [orderQuantity=" + orderQuantity + ", instrumentId=" + instrumentId + ", instrumentPrice="
				+ instrumentPrice + ", orderDate=" + orderDate + "]";
	}

	public int getOrderQuantity() {
		return orderQuantity;
	}

	public void setOrderQuantity(int orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	public String getInstrumentId() {
		return instrumentId;
	}

	public void setInstrumentId(String instrumentId) {
		this.instrumentId = instrumentId;
	}

	public double getInstrumentPrice() {
		return instrumentPrice;
	}

	public void setInstrumentPrice(double instrumentPrice) {
		this.instrumentPrice = instrumentPrice;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	  // Signifies that the market is open.
    public void openMarket()
    {
        System.out.println("Market opens.");
    }
    // Signifies that the market is Closed.
    public void closeMarket()
    {
        System.out.println("Market closes.");
     }

  
}
