package com.bos.book.model;


public class ResponseStatus {
	

	private String responseCode;
	private String responseMsg;
	
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMsg() {
		return responseMsg;
	}
	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ResponseStatus [responseCode=" + responseCode + ", responseMsg=" + responseMsg + "]";
	}


}