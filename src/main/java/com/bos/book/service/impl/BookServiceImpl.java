package com.bos.book.service.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.bos.book.model.Order;
import com.bos.book.service.BookOrderService;

@Service("bookorderService")
public class BookServiceImpl implements BookOrderService {
	
	 private static final Logger log = LoggerFactory.getLogger(BookServiceImpl.class);
	 
	   private Map<Double, List<Order>> bidMap = null;
	   
	   private Queue<Double> addBookOrderPriceList = null;
	   
	 
	 private Order orderbook;
	 
	    private Map<String, Order> orderBooksMap = null;
	 
	 public BookServiceImpl()
	    {
		 	orderBooksMap = new HashMap<String, Order>();
		 	orderbook = new Order("Test");
	        orderBooksMap.put("Test", orderbook);
	    }

	@Override
	public void orderBook(Order order) {
		 if (!orderBooksMap.containsKey(order.getInstrumentId())) {
			 orderbook = new Order(order.getInstrumentId());
			 orderBooksMap.put(order.getInstrumentId(), orderbook);
			 
			 addOrder(order.getOrderQuantity(),order.getInstrumentPrice());
	      }
		
	}

	@Override
	public void cancelBook(Order order) {
		if (orderBooksMap.containsKey(order.getInstrumentId())) {
			 orderbook = new Order(order.getInstrumentId());
			 orderBooksMap.remove(order.getInstrumentId(),orderbook);
			 
			 CancelOrder(order.getOrderQuantity(),order.getInstrumentPrice());
	      }
		
	}

	@Override
	public void updateBook(Order order) {
		
		if (orderBooksMap.containsKey(order.getInstrumentId())) {
			 orderbook = new Order(order.getInstrumentId());
			 orderBooksMap.put(order.getInstrumentId(),orderbook);
	      }
		
		
	}

	  private void addOrder (int quantity,double price )
	    {
		  bidMap = new HashMap<Double, List<Order>>();
		  List<Order> bucket = getBucket(bidMap, price);
	        Order newOrder = new Order(quantity, price);
	        bucket.add(newOrder);
	        bidMap.put(newOrder.getInstrumentPrice(), bucket);
	        addBookOrderPriceList.add(price);
	    }
	  
	  private void CancelOrder (int quantity,double price )
	    {
		  bidMap = new HashMap<Double, List<Order>>();
		  List<Order> bucket = getBucket(bidMap, price);
	        Order newOrder = new Order(quantity, price);
	        bucket.remove(newOrder);
	        bidMap.remove(newOrder.getInstrumentPrice(), bucket);
	        addBookOrderPriceList.remove(price);
	    }


	  private List<Order> getBucket(Map<Double, List<Order>> hashmap, Double price)
	    {
	        List<Order> bucket;
	        if(hashmap.containsKey(price))
	        {
	            bucket = hashmap.get(price);
	        }
	        else
	        {
	            bucket = new LinkedList<Order>();
	        }
	        return bucket;
	    }
	  // Prints the remaining book order  from input map after close of market.
	    private void checkFailedOrders(Map<Double, List<Order>> hashmap, String type)
	    {
	        for (Double key : hashmap.keySet())
	        {
	            List<Order> bucket = hashmap.get(key);

	            for(Order order : bucket)
	            {
	                System.out.println(type + order.getOrderQuantity() + " shares for $" + order.getInstrumentPrice() + " per share failed Order.");
	            }
	        }
	    }

}
