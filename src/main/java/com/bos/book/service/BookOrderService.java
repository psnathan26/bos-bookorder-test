package com.bos.book.service;

import com.bos.book.model.Order;

public interface BookOrderService {
	
	void orderBook(Order order);
	void cancelBook(Order order);
	void updateBook(Order order);
	
}
