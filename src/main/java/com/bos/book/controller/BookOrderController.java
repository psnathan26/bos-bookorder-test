package com.bos.book.controller;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.bos.book.model.Order;
import com.bos.book.service.BookOrderService;

@RestController
public class BookOrderController extends ResponseEntityExceptionHandler {
	
	@Resource
	private BookOrderService bookOrderService;
	
	private static final Logger log = LoggerFactory.getLogger(BookOrderController.class);
	
	  @RequestMapping(value = "/bookOrder/add", method = RequestMethod.POST,consumes="application/json")
	  	public ResponseEntity<String> AddBookOrder(@RequestBody Order order) {
		  	log.info("Book Order Details: {}", order.toString());
		  	
		  	try {
    	
		  	bookOrderService.orderBook(order);
		  	}
		  	
		  	catch (Exception ex){
		  		
		  		log.error("Book Order Add error: {}", ex.getMessage());
		  	}
	    	
	  		return new ResponseEntity<String>("success: book Order is added..", HttpStatus.OK);
	  	}
	  
	  @RequestMapping(value = "/bookOrder/cancel", method = RequestMethod.POST,consumes="application/json")
	  	public ResponseEntity<String> CancelBookOrder(@RequestBody Order order) {
		  	log.info("Book Order Cancel Details: {}", order.toString());
		  	
		  	try {
  	
		  	bookOrderService.cancelBook(order);
		  	}
		  	
		  	catch (Exception ex){
		  		
		  		log.error("Book Order Cancel error: {}", ex.getMessage());
		  	}
	    	
	  		return new ResponseEntity<String>("success: book Order is Cancelled..", HttpStatus.OK);
	  	}

	  
	  
	
}
