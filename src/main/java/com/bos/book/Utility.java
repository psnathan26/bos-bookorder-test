package com.bos.book;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utility {

	private static final Logger log = LoggerFactory.getLogger(Utility.class);


	public static String getCurrentDateTime() {

		log.info("getCurrentDateTime:");
		DateFormat df = new SimpleDateFormat(BookConstants.DATE_TIME_FORMAT);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		log.info("getCurrentDateTime::" + df.format(timestamp));

		return df.format(timestamp);

	}
}
